#!/usr/bin/env node

require("colors");

const fs = require("fs");
const path = require("path");
const request = require("request-promise-native");
const shell = require("shelljs");

const TI_HOME = path.join(
  require("os").homedir(),
  "/Library/Application Support/Titanium/mobilesdk/osx"
);

function info(message) {
  console.log(message.green);
}

async function run([, , sdkVersion, reverse]) {
  if (!sdkVersion) {
    throw new Error("usage: ./run.js SDK_VERSION [-R] (eg: 8.0.0.GA)");
  }

  const sdkBase = path.join(TI_HOME, sdkVersion);
  if (!fs.existsSync(sdkBase)) {
    throw new Error(`${sdkBase} does not exists`);
  }

  if (reverse) {
    if (reverse !== "-R") {
      throw new Error(`unkown option: ${reverse}`);
    }
  }

  const PATH_COMMAND = "patch -b -p1" + (reverse ? " " + reverse : "");

  // patch 1

  info("fetching patch1...");
  const p1 = await request.get(
    "https://patch-diff.githubusercontent.com/raw/appcelerator/node-titanium-sdk/pull/80.patch"
  );

  info("applying patch1...");
  shell.cd(`${sdkBase}/node_modules/node-titanium-sdk`);
  shell.echo(p1).exec(PATH_COMMAND);

  // patch 2

  info("fetching patch2...");
  const p2 = await request.get(
    "https://patch-diff.githubusercontent.com/raw/appcelerator/titanium_mobile/pull/10809.patch"
  );

  info("applying patch2...");
  shell.cd(`${sdkBase}`);
  shell.echo(p2).exec(PATH_COMMAND);
}

run(process.argv)
  .then(() => console.log("done!".yellow))
  .catch(err => console.error(`${err.message}`.red));
