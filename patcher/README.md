## install

`npm i`

## usage

apply patches to ti sdk `8.0.0.GA`:

`./run.js 8.0.0.GA`

revert patches to ti sdk `8.0.0.GA`:

`./run.js 8.0.0.GA -R`
